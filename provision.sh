#!/bin/bash

# Установка Docker

sudo apt-get update
sudo apt-get install -y \
ca-certificates \
curl \
gnupg \
lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker vagrant

# Установка Gitlab Runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install -y gitlab-runner
sudo usermod -aG docker gitlab-runner

# Регистрация http basic-auth
sudo -u vagrant mkdir /home/vagrant/auth
sudo -u vagrant docker run --entrypoint htpasswd httpd:2 -Bbn $login $pass > /home/vagrant/auth/htpasswd
chown vagrant:vagrant /home/vagrant/auth/htpasswd
# Создание local docker registry с помощью Docker compose

sudo -u vagrant mkdir /home/vagrant/data
sudo -u vagrant cat > /home/vagrant/docker-compose.yml <<EOF
version: "3"
services:
  registry:
    restart: always
    image: registry:2
    ports:
      - 5000:5000
    environment:
      REGISTRY_AUTH: htpasswd
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: ./data
      REGISTRY_AUTH_HTPASSWD_PATH: ./auth/htpasswd
      REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
    volumes:
      - ./auth:/auth
      - ./data:/data
EOF

chown vagrant:vagrant /home/vagrant/docker-compose.yml
cd /home/vagrant/ && sudo -u vagrant docker-compose up -d

# Установка и настройка конфига nginx
sudo apt-get install -y nginx
sudo ln -s /etc/nginx/sites-available/docker_registry_local /etc/nginx/sites-enabled/docker_registry_local
sudo unlink /etc/nginx/sites-enabled/default && sudo rm -rf /etc/nginx/sites-available/default

# Установка  сертификатов для ssl nginx

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -addext 'subjectAltName = IP:'$addr'/' -subj '/C=US/ST=CA/L=SanFrancisco/O=MyCompany/OU=RND/CN='$addr'\\/'
sudo openssl dhparam -out /etc/nginx/dhparam.pem 2048

# Добавление сертификата в Docker
sudo mkdir /etc/docker/certs.d 
sudo mkdir /etc/gitlab-runner/trusted-certs
sudo cp /etc/ssl/certs/nginx-selfsigned.crt /etc/docker/certs.d/nginx-selfsigned.crt
sudo cp /etc/ssl/certs/nginx-selfsigned.crt /etc/gitlab-runner/trusted-certs/nginx-selfsigned.crt
sudo systemctl restart docker
sudo systemctl restart gitlab-runner

# Настройка nginx для ssl
sudo cat > /etc/nginx/snippets/self-signed.conf <<EOF
ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
EOF

sudo cat > /etc/nginx/snippets/ssl-params.conf <<EOF
ssl_protocols TLSv1.3;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/nginx/dhparam.pem; 
ssl_ciphers EECDH+AESGCM:EDH+AESGCM;
ssl_ecdh_curve secp384r1;
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
EOF

sudo systemctl restart nginx

# Создание проекта в Gitlab и добавление раннеров

#sudo gitlab-runner register \
#  --non-interactive \
#  --url "https://gitlab.com/" \
#  --registration-token "$key_gitlab" \
#  --executor "docker" \
#  --tls-ca-file=/etc/gitlab-runner/trusted-certs/nginx-selfsigned.crt \
#  --docker-image alpine:latest \
#  --description "test" \
#  --maintenance-note "Free-form maintainer notes about this runner" \
#  --tag-list "docker" \
#  --run-untagged="true" \
#  --docker-privileged
  
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$key_gitlab" \
  --tls-ca-file=/etc/gitlab-runner/trusted-certs/nginx-selfsigned.crt \
  --executor "shell" \
  --docker-image ubuntu:latest \
  --description "test" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"